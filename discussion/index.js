console.log('Hello');

//array methods

// .push method will allow us to add an item at the end of array

let koponanNiEugene = ['Eugene'];

koponanNiEugene.push('vincent');
console.log(koponanNiEugene);

//the use of .push method also returns a value, the update length of the array

console.log(koponanNiEugene.push('Dennis'));
console.log(koponanNiEugene);

//.pop method allows us to remove an item at the end of the array and return the item that was deleted

let removeItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removeItem);

//.unshift() - unshift method allows us to add an item at the front of our array

let fruits = ['Mango', 'Kiwi', 'Apple'];
fruits.unshift('pineapple');
console.log(fruits);

//.shift() - allows us to remove an item at the beginning of our array

let computerBrands = ['Apple', 'Acer', 'Asus', 'Dell'];
computerBrands.shift();
console.log(computerBrands);


//splice() - simultaneusly remove elements from a specified index and add elements.

computerBrands.splice(1);
console.log(computerBrands);
//all items from the starting index has been deleted

//.splice(starting index, how many items to be deleted)
fruits.splice(0,1);
console.log(fruits);

//.splice(starting index, no deleted, elements to be add)
koponanNiEugene.splice(1,0,"Dennis", "Alfred")
console.log(koponanNiEugene);

//delete an indicating number of items to be deleted from the starting index and add elements
fruits.splice(0,2,'Lime', 'Cherry');
console.log(fruits);

let spiritDetective = koponanNiEugene.splice(0,1);
console.log(spiritDetective);
console.log(koponanNiEugene);

//.sort() - sort our elements in a an alphanumeric order
let members =['Ben', 'Allan','Alvin','Jino','Tine'];
members.sort();
console.log(members);
//note: .sort() method is itself used mostly for sorting arrays of string. It performs differently when sorting numbers.

let numbers =[50,100,12,10,1];
numbers.sort();
console.log(numbers);

//.reverse - reverses the order of the array elements
members.reverse();
console.log(members);

//non-mutator methods are not able to modify or change the original when they are used

let carBrands = ['Vios','Fortuner','Crosswind','City','Vios','Starex'];
//indexOf() - return the index number of the first element in the array. if no match found it will return negtive
let firstIndexOfVios = carBrands.indexOf("Vios");
console.log(firstIndexOfVios);

//very useful in finding the index of item when the total lenngth of the array is unknown
let indexOfStarex = carBrands.indexOf('Starex');
console.log(indexOfStarex);

//when the argument passed does not match item in the array. result: -1
let indexOfBeetle = carBrands.indexOf('Beetle');
console.log(indexOfBeetle);

//lastIndexOf() - returns the index of the last element in the array
let lastIndexOfVios = carBrands.lastIndexOf('Vios');
console.log(lastIndexOfVios);

let indexOfMio = carBrands.lastIndexOf('Mio');
console.log(indexOfMio);

//slice() copy and slice/portion of an array and return a new array from it
let shoeBrand = ['Jordan','Nike','Adidas','Converse','Sketchers'];

//slice(starting index) allows us to copy an array into a new array with items from the startingIndex to the last item
let myOwnShoes = shoeBrand.slice(1);
console.log(myOwnShoes);
console.log(shoeBrand);

//slice(starting index, ending index) = allows us to copy an array with items from the starting index to just before the ending index
let herOwnShoes = shoeBrand.slice(2,4);
console.log(herOwnShoes);

let heroes = ['Captain America','Superman','Spideman','Wonder Woman','Hulk','Hawkeye','Dr. Strange'];

let myFavoriteHeroes = heroes.slice(2,6);
console.log(myFavoriteHeroes);

//toString() - return our array into string separated by commas
let superHeroes = heroes.toString();
console.log(superHeroes);

//join() - returns our array as a string but this time by a specified separator

//without a specified separator
let superHeroes2 = heroes.join();
console.log(superHeroes2);

//with a specified separator (" ")
let superHeroes3= heroes.join(" ");
console.log(superHeroes3);

//with a specified separator (1)
let superHeroes4 = heroes.join(1);
console.log(superHeroes4);

//Iterator Method
//this method iterates or loops over the item in the array

//.forEach()
//similar to for loop wherein it is able to iterate over the items in an array.
// it is able to repeat an action FOR EACH item in the array

//.forEach() takes an argument which is a function. this function has no name and cannot be invoked outside .forEach()
//the function inside forEach is able to recieve the current item beeing looped
let counter = 0;
heroes.forEach(function(hero){
	counter++;
	console.log(counter);
	console.log(hero);
});

let chessBoard = [

	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']

]
	chessBoard.forEach(function(row){
		// console.log(row);
		row.forEach(function(square){
			console.log(square);
		});
	})


let numArr =[50,100,12,10,1];

numArr.forEach(function(number){
	if(number%5 ===0){
		console.log(number + ' is divisible by 5');
	}else {
		console.log(number + ' is not divisible by 5')
	}
})

//map() - similar to forEach() it will iterate over all item in an array and run function for each item. however, with a map, whatever is returned in the function will be added into a new array we can save
// let members =['Ben', 'Allan','Alvin','Jino','Tine'];
let instructors = members.map(function(member){
	return member + " is an instructor";

})
console.log(instructors);
console.log(members);

let numArr2 = [1,2,3,4,5];
let productMap = numArr2.map(function(number){
	return number * number;
})
console.log(productMap);

//map() vs forEach()
//map is able to return a new array
//foreach() simply iterates and does not return anything

//no return: undefined
let squarForEach = numArr2.forEach(function(number){
	return number * number;
})
console.log(squarForEach);

//includes()
//includes() - returns a boolean which determines if the item is in the arrayor not.

let isAMember = members.includes("Tine");
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);