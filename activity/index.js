console.log('hello');

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
console.log(registeredUsers);

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
    
let toRegister = prompt('Type in your name to register');
let checker = registeredUsers.indexOf(toRegister);
	if(checker === -1){
		alert('Thank you for registering');
	} else {
		alert('User already exists');
	}

	registeredUsers.push(toRegister);
	console.log('added: ' + toRegister);
	console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

let foundUser = friendsList.push(toRegister);

console.log(friendsList);
console.log('you have added ' + toRegister + " as a friend");

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

// // */
	// let counter = 0;
	// friendsList.forEach(friend){
	// counter++;
	// console.log(friend);
	// console.log(friendsList);

// }
	if(friendsList === -1){
		alert('you currently have 0 friends');
	} else {
		alert('you have ' + friendsList.indexOf() + 'friends');
	}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/
let numberOfFriends = friendsList.indexOf(foundUser);
	if(numberOfFriends === -1){
		console.log('Add one first');
	} else {
		console.log('you currently have ' + numberOfFriends + ' in your list');
	}

/*
    5. Create a function which will delete the last item you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/
let deleteItem = friendsList.pop();

	if(numberOfFriends === -1){
		alert('you have ' + numberOfFriends + 'friends');
	}else {
		alert('');
	}
	console.log(friendsList);


